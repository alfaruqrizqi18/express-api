-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 10 Okt 2018 pada 07.18
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `expressapi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `pub_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `pub_date`) VALUES
(1, 'MySQL for Beginner', 'Anjay Ramadhan', '2018-10-10 03:59:24'),
(2, 'MySQL for Medium', 'Young Lex', '2018-10-10 03:59:24'),
(3, 'MongoDB for Beginner', 'Rizqi Alfaruq', '2018-10-10 04:24:36'),
(4, 'MongoDB for Beginner', 'Rizqi Alfaruq', '2018-10-10 04:25:07'),
(5, 'MongoDB for Intermediate', 'Old Lex', '2018-10-10 04:28:43'),
(6, 'SQLite for Beginner', 'Old Lex', '2018-10-10 04:35:11'),
(7, 'SQLite for Medium', 'Old Lex', '2018-10-10 04:35:56'),
(8, 'SQLite for Super Intermediate', 'Old Lex', '2018-10-10 04:52:53'),
(9, 'Django for Super Intermediate', 'Old Lex', '2018-10-10 04:55:47'),
(10, 'Django for Super Intermediate 2', 'Old Lex', '2018-10-10 04:58:15'),
(11, 'Django for Super Intermediate 3', 'Old Lex', '2018-10-10 04:59:39'),
(12, 'Django for Super Intermediate 4', 'Old Lex', '2018-10-10 05:06:14'),
(13, 'Django for Super Intermediate 5', 'Old Lex', '2018-10-10 05:07:39'),
(14, 'Django for Super Intermediate 6', 'Old Lex', '2018-10-10 05:14:07'),
(15, 'Django for Super Intermediate 7', 'Old Lex', '2018-10-10 05:15:24');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
