var express = require("express");
var router = express.Router();
var pool = require("../database/config");
const save = require("../interraction/save"),
  fetch = require("../interraction/fetch");
let queryString;
/* GET home page. */
router.get("/", (req, res, next) => {
  queryString = "select * from books order by id desc";
  fetch(queryString, res);
});

router.post("/", (req, res) => {
  let postData = {
    title: req.body.title,
    author: req.body.author
  };
  queryString = "insert into books SET ?";
  save(queryString, res, postData);
});

router.get("/:id", (req, res) => {
  let bookId = req.params.id;
  queryString = "select * from books where id=" + bookId;
  fetch(queryString, res);
});
module.exports = router;
