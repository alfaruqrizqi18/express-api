var pool = require("../database/config");

let save = function(queryString, res, param) {
  let returnData;
  pool.query(queryString, param, function(err, result, fields) {
    if (err) throw new Error(err);
    returnData = res.json({
      message: "1 book inserted",
      data: param
    });
  });
  return returnData;
};

module.exports = save;
