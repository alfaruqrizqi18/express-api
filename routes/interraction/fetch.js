var pool = require("../database/config");

let fetch = function(queryString, res) {
  let returnData;
  pool.query(queryString, function(err, result, fields) {
    if (err) throw new Error(err);
    // Do something with result.
    if (result.length == 0)
      returnData = res.json({
        message: "Data not found"
      });
    else
      returnData = res.json({
        message: "Data found",
        rows: result.length,
        data: result
      });
  });
  return returnData;
};

module.exports = fetch;
